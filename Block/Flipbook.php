<?php
namespace Magebees\Flipbook\Block;
class Flipbook extends \Magento\Framework\View\Element\Template
{
	private $objectManager;
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magebees\Flipbook\Model\BookFactory $bookFactory,
		\Magento\Framework\ObjectManagerInterface $objectmanager
    ) {
        $this->bookFactory = $bookFactory;
		$this->objectManager = $objectmanager;
        parent::__construct($context);
    }
    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        $this->pageConfig->getTitle()->set(__('Flipbooks'));
        $getBooksCount = '';
        $getBooksCount = $this->getBooksCount()+12;
        if ($this->getAllBooks()) {
            $pager = $this->getLayout()->createBlock(
                'Magento\Theme\Block\Html\Pager',
                'flipbook.new.pager'
            )->setAvailableLimit(array(4=>4,8=>8,12=>12,$getBooksCount=>'All'))->setShowPerPage(true)->setCollection(
                $this->getAllBooks()
            );
            $this->setChild('pager', $pager);
            $this->getAllBooks()->load();
        }
        return $this;
    }

    public function getPagerHtml()
    {
        return $this->getChildHtml('pager');
    }

    public function getBooksCount()
    {
        $book = $this->bookFactory->create()->getCollection()->addFieldToFilter('status', 1);
        return count($book);
    }

    public function getAllBooks()
    {
        $bookpage=($this->getRequest()->getParam('p'))? $this->getRequest()->getParam('p') : 1;
        $bookpageSize=($this->getRequest()->getParam('limit'))? $this->getRequest()->getParam('limit') : 4;
        $now_book = new \DateTime();
        $book_data = $this->bookFactory->create()->getCollection()
					->addFieldToFilter('status', 1)
					->addFieldToFilter('from_date',[['lteq' => $now_book->format('Y-m-d H:i:s')],['null' => true]])
					->addFieldToFilter('to_date',[['gteq' => $now_book->format('Y-m-d H:i:s')],['null' => true]])
					->setPageSize($bookpageSize)
					->setCurPage($bookpage);
        return $book_data;
    }

	public function getAllCodeBooks()
    {
        $now_book = new \DateTime();
        $book_data = $this->bookFactory->create()->getCollection()
					->addFieldToFilter('status', 1)
					->addFieldToFilter('from_date',[['lteq' => $now_book->format('Y-m-d H:i:s')],['null' => true]])
					->addFieldToFilter('to_date',[['gteq' => $now_book->format('Y-m-d H:i:s')],['null' => true]]);
        return $book_data;
    }

	public function getAllCatBooks($catId)
    {
        return [];
        $now_book = new \DateTime();
        $book_data = $this->bookFactory->create()->getCollection()
					->addFieldToFilter('status', 1)
					->addFieldToFilter('categories', array('regexp' => '[[:<:]]'.$catId.'[[:>:]]'))
					->addFieldToFilter('from_date',[['lteq' => $now_book->format('Y-m-d H:i:s')],['null' => true]])
					->addFieldToFilter('to_date',[['gteq' => $now_book->format('Y-m-d H:i:s')],['null' => true]]);
		return $book_data;
    }

	public function getCurrentCategory(){
		$category = $this->objectManager->get('Magento\Framework\Registry')->registry('current_category');
		return $category->getId();
	}
}
