<?php
namespace Magebees\Flipbook\Block;
class ProductFlipbookBlock extends \Magento\Framework\View\Element\Template
{
    private $objectManager;    
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magebees\Flipbook\Model\ProductFactory $bookproduct,
        \Magebees\Flipbook\Model\BookFactory $bookFactory,
		\Magento\Framework\ObjectManagerInterface $objectmanager
    ) {
        $this->bookproduct = $bookproduct;
        $this->bookFactory = $bookFactory;
		$this->objectManager = $objectmanager;
        parent::__construct($context);
    }
    public function getProductBook($product_id)
    {   
        $products = $this->bookproduct->create()->getCollection()->addFieldToFilter('product_id', $product_id);
        return $products;
    }
    public function getBooksForProduct($productbooks)
    {
        $book_data = [];
        foreach ($productbooks as $id) {        
            $book = $this->bookFactory->create()->load($id->getBookId());
            $book_data[] = $book->getData();
        }
        return $book_data;
    }
    public function getBooks()
    {
        $book = $this->bookFactory->create()->getCollection()->addFieldToFilter('status', 1);
        $book_data = $book->getData();
        return $book_data;
    }	
	public function getCurrentProduct(){
		return $this->objectManager->get('Magento\Framework\Registry')->registry('current_product');
	}
}