<?php
namespace Magebees\Flipbook\Block;
class Flipbookview extends \Magento\Framework\View\Element\Template
{
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magebees\Flipbook\Model\BookFactory $bookFactory
    ) {
        $this->bookFactory = $bookFactory;
        parent::__construct($context);
    }

    protected function _prepareLayout()
    {
        parent::_prepareLayout(); 
        return $this;
    }

	public function getBookData($id)
    {
		$now_book = new \DateTime();
        $book_data = $this->bookFactory->create()->getCollection()
					->addFieldToFilter('book_id', $id)
					->addFieldToFilter('status', 1)
					->addFieldToFilter('from_date',[['lteq' => $now_book->format('Y-m-d H:i:s')],['null' => true]])
					->addFieldToFilter('to_date',[['gteq' => $now_book->format('Y-m-d H:i:s')],['null' => true]])
					->getFirstItem();
        return $book_data->getData();
    }
	public function getBookMetaData($id)
    {
		$now_book = new \DateTime();
        $book_data = $this->bookFactory->create()->getCollection()
					->addFieldToSelect('meta_title')
					->addFieldToSelect('meta_keywords')
					->addFieldToSelect('meta_description')
					->addFieldToFilter('book_id', $id)
					->addFieldToFilter('status', 1)
					->addFieldToFilter('from_date',[['lteq' => $now_book->format('Y-m-d H:i:s')],['null' => true]])
					->addFieldToFilter('to_date',[['gteq' => $now_book->format('Y-m-d H:i:s')],['null' => true]])
					->getFirstItem();
        return $book_data->getData();
    }
	public function getCodeBookData()
    {
		$id = $this->getCode();
		$now_book = new \DateTime();
        $book_data = $this->bookFactory->create()->getCollection()
					->addFieldToFilter('book_id', $id)
					->addFieldToFilter('status', 1)
					->addFieldToFilter('from_date',[['lteq' => $now_book->format('Y-m-d H:i:s')],['null' => true]])
					->addFieldToFilter('to_date',[['gteq' => $now_book->format('Y-m-d H:i:s')],['null' => true]])
					->getFirstItem();
        return $book_data->getData();
    }
}

