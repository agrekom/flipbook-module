<?php
namespace Magebees\Flipbook\Block\Adminhtml;
class Book extends \Magento\Backend\Block\Widget\Grid\Container
{
    protected function _construct()
    {
        $this->_controller = 'adminhtml_book';
        $this->_blockGroup = 'Magebees_Flipbook';
        $this->_headerText = __('Flipbooks');
        $this->_addButtonLabel = __('Add New Book');
        parent::_construct();
    }
}