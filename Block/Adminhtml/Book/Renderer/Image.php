<?php
namespace Magebees\Flipbook\Block\Adminhtml\Book\Renderer;
use Magento\Framework\UrlInterface;
class Image extends \Magento\Framework\Data\Form\Element\Image
{
    public function getElementHtml()
    {
        $html = '';
        $img_url = $this->getValue();
        if ($img_url) {
            $url = $this->_getUrl();
            if (!preg_match("/^http\:\/\/|https\:\/\//", $url)) {
                $url = $this->_urlBuilder->getBaseUrl(['_type' => UrlInterface::URL_TYPE_MEDIA]) . $url;
            }
            $url = $this->_escaper->escapeUrl($url);
            $html = '<a href="' .
                $url .
                '"' .
                ' onclick="imagePreview(\'' .
                $this->getHtmlId() .
                '_image\'); return false;" ' .
                $this->_getUiId(
                    'link'
                ) .
                '> <img src="'.$url.'" id="thumbnail_book_image" height="25" width="25" class="small-image-preview v-middle">' .
                '</a> ';
        }
        if(empty($img_url)) {
            $this->setClass('input-file required-entry _required');
        }else{
            $this->setClass('input-file');
        }
        $html .= \Magento\Framework\Data\Form\Element\AbstractElement::getElementHtml();
        return $html;
    }
}