<?php
namespace Magebees\Flipbook\Block\Adminhtml\Book\Grid\Renderer;
class Image extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer
{
    protected $_storeManager;
    public function __construct(
        \Magento\Backend\Block\Context $context,
        \Magento\Store\Model\StoreManagerInterface $storeManager,      
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->_storeManager = $storeManager;        
    }	
    public function render(\Magento\Framework\DataObject $row)
    {
        $thumbnail = $row->getThumbnail();   
        $mediaDirectory = $this->_storeManager->getStore()->getBaseUrl(
            \Magento\Framework\UrlInterface::URL_TYPE_MEDIA
        );	
        $img = '';
        if($thumbnail) {
            $img = "<img src='".$mediaDirectory.'catalog/product/'.$thumbnail."' height='100px' width='100px'>";
        }else{
             $img='<img src="'.$mediaDirectory.'flipbook/no-img.jpg'.'" width="100px" height="100px"/>';
        } 
        return $img;
    }
}