<?php
namespace Magebees\Flipbook\Block\Adminhtml\Book;
class Grid extends \Magento\Backend\Block\Widget\Grid\Extended
{
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        \Magebees\Flipbook\Model\BookFactory $bookFactory,
        array $data = []
    ) {
        $this->_bookFactory = $bookFactory;
        parent::__construct($context, $backendHelper, $data);
    }
    protected function _construct()
    {
        parent::_construct();
        $this->setId('booksGrid');
        $this->setDefaultSort('book_id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
    }
    protected function _prepareCollection()
    {
        $collection = $this->_bookFactory->create()->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }
        
    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('book_id');
        $this->getMassactionBlock()->setFormFieldName('books');   
        $this->getMassactionBlock()->addItem(
            'display',
            [
            'label' => __('Delete'),
            'url' => $this->getUrl('*/*/massdelete'),
            'confirm' => __('Are you sure?'),
            'selected'=>true
            ]
        );
        
        $status = [
            ['value' => 1, 'label'=>__('Active')],
            ['value' => 0, 'label'=>__('Inactive')],
        ];

        array_unshift($status, ['label'=>'', 'value'=>'']);
        $this->getMassactionBlock()->addItem(
            'status',
            [
                'label' => __('Change status'),
                'url' => $this->getUrl('*/*/massstatus', ['_current' => true]),
                'additional' => [
                    'visibility' => [
                        'name' => 'status',
                        'type' => 'select',
                        'class' => 'required-entry',
                        'label' => __('Status'),
                        'values' => $status
                    ]
                ]
            ]
        );        
        return $this;
    }
    protected function _prepareColumns()
    {
        $this->addColumn(
            'book_id',
            [
                'header' => __('Book Id'),
                'type' => 'number',
                'index' => 'book_id',
                'header_css_class' => 'col-id',
                'column_css_class' => 'col-id',
            ]
        );
        $this->addColumn(
            'book_title',
            [
                'header' => __('Book Title'),
                'index' => 'book_title',
            ]
        );
        $this->addColumn(
            'author_name',
            [
                'header' => __('Author Name'),
                'index' => 'author_name',
            ]
        );
        
        $this->addColumn(
            'created_at',
            [
                'header' => __('Created At'),
                'index' => 'created_at',
            ]
        );
        
        $this->addColumn(
            'status',
            [
                'header' => __('Status'),
                'index' => 'status',
                'frame_callback' => [$this, 'decorateStatus'],
                'type' => 'options',
                'options' => [ '0' => 'Inactive', '1' => 'Active'],
            ]
        );
        
        $this->addColumn(
            'edit',
            [
                'header' => __('Edit'),
                'type' => 'action',
                'getter' => 'getId',
                'actions' => [
                    [
                        'caption' => __('Edit'),
                        'url' => [
                            'base' => '*/*/edit',
                            'params' => ['store' => $this->getRequest()->getParam('store')]
                        ],
                        'field' => 'id'
                    ]
                ],
                'filter' => false,
                'sortable' => false,
                'index' => 'stores',
                'header_css_class' => 'col-action',
                'column_css_class' => 'col-action'
            ]
        );

        $this->addColumn(
            'delete',
            [
                'header' => __('Delete'),
                'type' => 'action',
                'getter' => 'getId',
                'actions' => [
                    [
                        'caption' => __('Delete'),
                        'url' => [
                            'base' => '*/*/delete',
                            'params' => ['store' => $this->getRequest()->getParam('store')]
                        ],
                        'field' => 'id'
                    ]
                ],
                'filter' => false,
                'sortable' => false,
                'index' => 'stores',
                'header_css_class' => 'col-action',
                'column_css_class' => 'col-action'
            ]
        );   
        return parent::_prepareColumns();
    }
    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', ['_current' => true]);
    }
    public function getRowUrl($row)
    {
        return $this->getUrl(
            '*/*/edit',
            ['id' => $row->getId()]
        );
    }
    public function decorateStatus($value, $row, $column, $isExport)
    {
        if ($value=='Active') {
            $cell = '<span class="grid-severity-notice"><span>Active</span></span>';
        } else {
            $cell = '<span class="grid-severity-minor"><span>Inactive</span></span>';
        }
        return $cell;
    }
	static public function getOptionArray(){
		$data_array = array();
		$data_array[1] = __('As a New Tab (Beside Review Tab)');
		$data_array[2] = __('Top Right Corner on Product Image');
		$data_array[3] = __('Under Add to Wishlist Button');
		return($data_array);
	}
	static public function getValueArray(){
		$data_array=array();
		foreach(\Magebees\Flipbook\Block\Adminhtml\Book\Grid::getOptionArray() as $k=>$v){
			$data_array[]=array('value'=>$k,'label'=>$v);
		}
		return($data_array);
	}	
}