<?php
namespace Magebees\Flipbook\Block\Adminhtml\Book\Edit\Tab;
use Magento\Backend\Block\Template\Context;
use Magento\Backend\Block\Widget\Form\Generic;
use Magento\Framework\Data\FormFactory;
use Magento\Framework\Registry;
class ShowButton extends \Magento\Backend\Block\Widget\Form\Generic
{
    public function __construct(
        Context $context,
        Registry $registry,
        FormFactory $formFactory,
        \Magento\Rule\Block\Conditions $conditions,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Magento\Backend\Block\Widget\Form\Renderer\Fieldset $rendererFieldset,
        array $data = []
    ) {
        $this->_conditions = $conditions;
        $this->_rendererFieldset = $rendererFieldset;
        
        parent::__construct($context, $registry, $formFactory, $data);
    }
    protected function _prepareForm()
    {
        $model = $this->_coreRegistry->registry('books_data');           
        $form = $this->_formFactory->create();
		$fieldset = $form->addFieldset('showhide_fieldset', ['legend' => __('Show/Hide Icons')]);
        $fieldset->addField(
            'show_toc',
            'select',
            [
                'name' => 'show_toc',
                'label' => __('Show Table of Contents Icon'),
                'title' => __('Show Table of Contents Icon'),
                'value' => 1,
                'values' => array( array('label' => 'Yes', 'value' => '1'), array('label' => 'No', 'value' => '0')),
            ]
        );
        $fieldset->addField(
            'show_thumbnail',
            'select',
            [
                'name' => 'show_thumbnail',
                'label' => __("Show Thumbnail's Icon"),
                'title' => __("Show Thumbnail's Icon"),
                'value' => 1,
                'values' => array( array('label' => 'Yes', 'value' => '1'), array('label' => 'No', 'value' => '0')),
            ]
        );        
        $fieldset->addField(
            'show_print',
            'select',
            [
                'name' => 'show_print',
                'label' => __('Show Print Icon'),
                'title' => __('Show Print Icon'),
                'value' => 1,
                'values' => array( array('label' => 'Yes', 'value' => '1'), array('label' => 'No', 'value' => '0')),
            ]
        );
        $fieldset->addField(
            'show_download',
            'select',
            [
                'name' => 'show_download',
                'label' => __('Show Download Icon'),
                'title' => __('Show Download Icon'),
                'value' => 1,
                'values' => array( array('label' => 'Yes', 'value' => '1'), array('label' => 'No', 'value' => '0')),
            ]
        );
        $fieldset->addField(
            'show_toggle_sound',
            'select',
            [
                'name' => 'show_toggle_sound',
                'label' => __('Show Toggle Sound Icon'),
                'title' => __('Show Toggle Sound Icon'),
                'value' => 1,
                'values' => array( array('label' => 'Yes', 'value' => '1'), array('label' => 'No', 'value' => '0')),
            ]
        );
        $fieldset->addField(
            'show_zoom',
            'select',
            [
                'name' => 'show_zoom',
                'label' => __('Show Zoom Icon'),
                'title' => __('Show Zoom Icon'),
                'value' => 1,
                'values' => array( array('label' => 'Yes', 'value' => '1'), array('label' => 'No', 'value' => '0')),
            ]
        );
        $fieldset->addField(
            'show_fullscreen',
            'select',
            [
                'name' => 'show_fullscreen',
                'label' => __('Show Fullscreen Icon'),
                'title' => __('Show Fullscreen Icon'),
                'value' => 1,
                'values' => array( array('label' => 'Yes', 'value' => '1'), array('label' => 'No', 'value' => '0')),
            ]
        );
        $model_data = $model->getData();
        $form->setValues($model_data);
        $this->setForm($form);
            
        return parent::_prepareForm();
    }
    protected function _isAllowedAction($resourceId)
    {
        return $this->_authorization->isAllowed($resourceId);
    }
}
