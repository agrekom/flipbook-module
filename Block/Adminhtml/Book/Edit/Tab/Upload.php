<?php
namespace Magebees\Flipbook\Block\Adminhtml\Book\Edit\Tab;
class Upload extends \Magento\Backend\Block\Widget\Form\Generic
{
    protected $_template = 'tab/upload.phtml'; 
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magebees\Flipbook\Model\BookFactory $bookFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magebees\Flipbook\Model\BookimageFactory $bookImageFactory,
        array $data = []
    ) {

        $this->bookFactory = $bookFactory;
        $this->storeManager = $storeManager;
        $this->bookImageFactory = $bookImageFactory;
        parent::__construct($context, $registry, $formFactory, $data);
    }
    protected function _prepareLayout()
    {
        $this->addChild(
            'add_button',
            \Magento\Backend\Block\Widget\Button::class,
            ['label' => __('Add New'), 'class' => '', 'onclick' => 'flipbook_images.add()']
        );

        return parent::_prepareLayout();
    }
    public function getSelectedMode()
    {
        $book_id = $this->getRequest()->getParam('id')?$this->getRequest()->getParam('id'):0;
        $book_data = $this->bookFactory->create()->load($book_id);
        $mode_upload = $book_data->getModeOfUpload();
        return $mode_upload;
    }
    public function getPdfUploded()
    {
        $book_id = $this->getRequest()->getParam('id')?$this->getRequest()->getParam('id'):0;
        $book_data = $this->bookFactory->create()->load($book_id);
        $pdf_upload = $book_data->getBookPdf();       
        $currentStore = $this->storeManager->getStore();
        $mediaUrl = $currentStore->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
        if(!empty($pdf_upload)) {
            return $mediaUrl.$pdf_upload;
        }else{
            return ;
        }
    }
    public function getflipbook_image_JsonData() 
    {    
        $book_id = $this->getRequest()->getParam('id')?$this->getRequest()->getParam('id'):0;       
        $images_data = $this->bookImageFactory->create()->getCollection()->addFieldToFilter("book_id", $book_id)->getData();
        if(isset($images_data) && !empty($images_data)) {
            foreach($images_data as $img){                
                $new['book_image'] = $img['image'];
                $new['image_id'] = $img['image_id'];
                $new['sort_order'] = $img['sort_order'];
                $new['notes'] = $img['notes'];
                $this->images_data_new[] = json_encode($new);
            }
            return $this->images_data_new;
        }else{
            return ;
        }
    }
    public function getflipbook_image_cover()
    {
        $book_id = $this->getRequest()->getParam('id')?$this->getRequest()->getParam('id'):0;
        $cover_image = $this->bookFactory->create()->load($book_id);
        $currentStore = $this->storeManager->getStore();
        $mediaUrl = $currentStore->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);   
        if(isset($cover_image) && !empty($cover_image)) {
            return $mediaUrl.$cover_image->getCoverBook();
        }else{
            return ;
        }
    }
    public function getflipbook_image_last()
    {
        $book_id = $this->getRequest()->getParam('id')?$this->getRequest()->getParam('id'):0;
        $cover_image = $this->bookFactory->create()->load($book_id);
        $currentStore = $this->storeManager->getStore();
        $mediaUrl = $currentStore->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);        
        if(isset($cover_image) && !empty($cover_image)) {
            return $mediaUrl.$cover_image->getLastBook();
        }else{
            return ;
        }
    }
    public function getMediaUrl()
    {
        $currentStore = $this->storeManager->getStore();
        $mediaUrl = $currentStore->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
        return $mediaUrl;
    }
    protected function _isAllowedAction($resourceId)
    {
        return $this->_authorization->isAllowed($resourceId);
    }
    public function getImagesData()
    {
        $book_id = $this->getRequest()->getParam('id')?$this->getRequest()->getParam('id'):0;       
        $images_data = $this->bookImageFactory->create()->getCollection()->addFieldToFilter("book_id", $book_id);
        $images_data = $images_data->setOrder('sort_order', 'ASC');
        $mediaUrl = $this->getMediaUrl();
        foreach ($images_data as $img) {
            $img['image'] = $mediaUrl.$img['image'];
        }
        return $images_data;
    }
    public function getBookdata()
    {
        $book_id = $this->getRequest()->getParam('id')?$this->getRequest()->getParam('id'):0;
        $data = $this->bookFactory->create()->load($book_id);
        return $data;
    }
}