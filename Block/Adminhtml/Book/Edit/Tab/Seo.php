<?php
namespace Magebees\Flipbook\Block\Adminhtml\Book\Edit\Tab;
class Seo extends \Magento\Backend\Block\Widget\Form\Generic
{
    protected $_systemStore;  
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\Store\Model\System\Store $systemStore,
        array $data = []
    ) {
        $this->_systemStore = $systemStore;
        parent::__construct($context, $registry, $formFactory, $data);
    }
     
    protected function _prepareForm()
    {
        $model = $this->_coreRegistry->registry('books_data');          
        $form = $this->_formFactory->create();
        $fieldset = $form->addFieldset('base_fieldset', ['legend' => __('Search Engine Optimization (This will be shown on Flipbook Individual Page Only)')]);

        if ($model->getId()) {
            $fieldset->addField('book_id', 'hidden', ['name' => 'book_id']);
        }

        $fieldset->addField(
            'meta_title',
            'text',
            [
                'name' => 'meta_title',
                'label' => __('Meta Title'),
                'title' => __('Meta Title'),
                'required' => false,
            ]
        );
		$fieldset->addField(
            'meta_keywords',
            'textarea',
            [
                'name' => 'meta_keywords',
                'label' => __('Meta Keywords'),
                'title' => __('Meta Keywords'),
                'required' => false,
            ]
        );
        $fieldset->addField(
            'meta_description',
            'textarea',
            [
                'name' => 'meta_description',
                'label' => __('Meta Description'),
                'title' => __('Meta Description'),
                'required' => false,
            ]
        );
		
        $model_data = $model->getData();
        $form->setValues($model_data);
        $this->setForm($form);
        
        return parent::_prepareForm();
    }
    protected function _isAllowedAction($resourceId)
    {
        return $this->_authorization->isAllowed($resourceId);
    }
}