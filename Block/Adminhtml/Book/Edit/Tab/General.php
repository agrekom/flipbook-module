<?php
namespace Magebees\Flipbook\Block\Adminhtml\Book\Edit\Tab;
class General extends \Magento\Backend\Block\Widget\Form\Generic
{
    protected $_systemStore;  
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\Store\Model\System\Store $systemStore,
        array $data = []
    ) {
        $this->_systemStore = $systemStore;
        parent::__construct($context, $registry, $formFactory, $data);
    }
     
    protected function _prepareForm()
    {
        $model = $this->_coreRegistry->registry('books_data');          
        $form = $this->_formFactory->create();
        $fieldset = $form->addFieldset('base_fieldset', ['legend' => __('General Information')]);

        if ($model->getId()) {
            $fieldset->addField('book_id', 'hidden', ['name' => 'book_id']);
        }

        $fieldset->addField(
            'book_title',
            'text',
            [
                'name' => 'book_title',
                'label' => __('Book Name'),
                'title' => __('Book Name'),
                'required' => true,
            ]
        );

        $fieldset->addField(
            'author_name',
            'text',
            [
                'name' => 'author_name',
                'label' => __('Author Name'),
                'title' => __('Author Name'),
                'required' => true,
            ]
        );

        $fieldset->addType(
            'cs_image',
            'Magebees\Flipbook\Block\Adminhtml\Book\Renderer\Image'
        );

        $fieldset->addField(
            'thumbnail_book', 
            'cs_image', 
            [
                  'label' => __('Book Thumbnail'),
                  'title' => __('Book Thumbnail'),
                  'required' => true,
                  'name'  => 'thumbnail_book',
                  'note' => 'Allowed file types : jpg, jpeg, png, gif.',
            ]
        );

        $fieldset->addField(
            'description',
            'textarea',
            [
                'name' => 'description',
                'label' => __('Description'),
                'title' => __('Description'),
            ]
        );

        $fieldset->addField(
            'status',
            'select',
            [
                'name' => 'status',
                'label' => __('Status'),
                'title' => __('Status'),
                'values' => [
                    '1' => __('Active'),
                    '0' => __('Inactive'),
                ],
            ]
        );

        $add_product = $fieldset->addField(
            'add_product',
            'select',
            [
                'name' => 'add_product',
                'label' => __('Show Flipbook To Product Page'),
                'title' => __('Show Flipbook To Product Page'),
                'values' => [
                    '1' => __('Yes'),
                    '0' => __('No'),
                ],
                'note' => __('If select yes, Please Select Products form Product Section.'), 
            ]
        );
        
        $book_position = $fieldset->addField(
            'position_on_product_page',
            'multiselect',
            [
                'name' => 'position_on_product_page',
                'label' => __('Position in Product Page'),
                'title' => __('Position in Product Page'),
		        'values' => \Magebees\Flipbook\Block\Adminhtml\Book\Grid::getValueArray(),
                'required' => true,
            ]
        );

        $this->setChild(
            'form_after',
            $this->getLayout()->createBlock('\Magento\Backend\Block\Widget\Form\Element\Dependence')
                ->addFieldMap($add_product->getHtmlId(), $add_product->getName())
                ->addFieldMap($book_position->getHtmlId(), $book_position->getName())
                ->addFieldDependence($book_position->getName(), $add_product->getName(), 1)
        );

        $fieldset->addField(
            'from_date',
            'date',
            [
                'name' => 'from_date',
                'label' => __('Start Date'),
                'id' => 'holiday_date',
                'title' => __('Start Date'),
                'date_format' => 'yyyy-MM-dd',
                "note"      => "(YY-MM-DD)",          
            ]
        );
        
        $fieldset->addField(
            'to_date',
            'date',
            [
                'name' => 'to_date',
                'label' => __('End Date'),
                'id' => 'to_date',
                'title' => __('Holiday Date'),
                'date_format' => 'yyyy-MM-dd',
                "note"      => "(YY-MM-DD)",          
            ]
        );

        $model_data = $model->getData();
        $form->setValues($model_data);
        $this->setForm($form);
        
        return parent::_prepareForm();
    }
    protected function _isAllowedAction($resourceId)
    {
        return $this->_authorization->isAllowed($resourceId);
    }
}