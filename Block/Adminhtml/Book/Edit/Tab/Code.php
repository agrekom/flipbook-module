<?php
namespace Magebees\Flipbook\Block\Adminhtml\Book\Edit\Tab;
class Code extends \Magento\Backend\Block\Widget\Form\Generic implements \Magento\Backend\Block\Widget\Tab\TabInterface
{
    protected $_systemStore;
	protected $_responsivebannerslider;

    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\Store\Model\System\Store $systemStore,
        array $data = array()
    ) {
        $this->_systemStore = $systemStore;
		$this->setTemplate('Magebees_Flipbook::tab/code.phtml');		
        parent::__construct($context, $registry, $formFactory, $data);
    }

    protected function _prepareForm()
    {	
        return parent::_prepareForm();   
    } 

    public function getTabLabel()
    {
        return __('Use Code Inserts');
    }

    public function getTabTitle()
    {
        return __('Use Code Inserts');
    }

	public function getCurrentBook() {
		$model = $this->_coreRegistry->registry('books_data');
		return $model->getData();    
	}

    public function canShowTab()
    {
		$model = $this->_coreRegistry->registry('books_data'); 
		if ($model->getId()) {
           return true;
        }else{
			return false;
		}	
    }
    public function isHidden()
    {
        return false;
    }
    protected function _isAllowedAction($resourceId)
    {
        return $this->_authorization->isAllowed($resourceId);
    }
}
