<?php
namespace Magebees\Flipbook\Block\Adminhtml\Book\Edit;
class Tabs extends \Magento\Backend\Block\Widget\Tabs
{
    protected function _construct()
    {
        parent::_construct();
        $this->setId('book_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(__('Book Information'));
    }    
    protected function _prepareLayout()
    {        
        $this->addTab(
            'general_section',
            [
                'label' => __('General Information'),
                'title' => __('General Information'),
                'content' => $this->getLayout()->createBlock(
                    'Magebees\Flipbook\Block\Adminhtml\Book\Edit\Tab\General'
                )->toHtml(),
                'active' => true
            ]
        );   
        $this->addTab(
            'upload_section',
            [
                'label' => __('Mode of Upload Book Details (PDF/Images)'),
                'title' => __('Mode of Upload Book Details (PDF/Images)'),
                'content' => $this->getLayout()->createBlock(
                    'Magebees\Flipbook\Block\Adminhtml\Book\Edit\Tab\Upload'
                )->toHtml()
            ]
        );
        $this->addTab(
            'show_hide_section',
            [
                'label' => __('Show / Hide Icons'),
                'title' => __('Show / Hide Icons'),
                'content' => $this->getLayout()->createBlock(
                    'Magebees\Flipbook\Block\Adminhtml\Book\Edit\Tab\ShowButton'
                )->toHtml()
            ]
        );
        $this->addTab(
            'product_section',
            [
                'label' => __('Select Products'),
                'title' => __('Select Products'),
                'url' => $this->getUrl('flipbook/book/producttab', ['_current' => true]),
                'class' => 'ajax'
            ]
        );
		$this->addTab('categories_section',
			[
				'label' => __('Select Categories'),
				'title' => __('Select Categories'),
				'content' => $this->getLayout()->createBlock(
				'Magebees\Flipbook\Block\Adminhtml\Book\Edit\Tab\Categories')->toHtml(),
				'active' => false,
				'class' => 'ajax'
			]
		);
		$this->addTab(
            'seo',
            [
                'label' => __('Search Engine Optimization'),
                'title' => __('Search Engine Optimization'),
                'content' => $this->getLayout()->createBlock(
                    'Magebees\Flipbook\Block\Adminhtml\Book\Edit\Tab\Seo'
                )->toHtml()
            ]
        );
        return parent::_prepareLayout();
    }
}