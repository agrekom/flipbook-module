<?php 
namespace Magebees\Flipbook\Block\Widget;
use Magento\Framework\View\Element\Template;
use Magento\Widget\Block\BlockInterface; 

class Book extends Template implements BlockInterface
{
    protected $_template = "widget/book.phtml";
    const BOOK_PDF = 1;
    const BOOK_IMAGES = 2;

    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magebees\Flipbook\Model\BookFactory $bookFactory,
        array $data = []
    ) {
        $this->_bookFactory = $bookFactory;
        parent::__construct($context, $data); 
    }
    public function setWidgetOptions()
    {   
        $this->setFlipbookId($this->getFlipbookId());
        $this->setFlipbookViewType($this->getFlipbookViewType());              
    }
    protected function _beforeToHtml()
    {
        $this->setWidgetOptions();
        return parent::_beforeToHtml();
    }
    public function getBookforWidget($id)
    {
        $book_data = $this->_bookFactory->create()->load($id);
        $mode_of_upload = $book_data->getModeOfUpload();   
        return $book_data;
    }
	public function getAllBooks($id)
    {
		$ids = explode(",",$id);
        $now_book = new \DateTime();
        $book_data = $this->_bookFactory->create()->getCollection()
					->addFieldToFilter('book_id', array('in' => $ids))
					//->addFieldToFilter('book_id', $id)
					->addFieldToFilter('status', 1)
					->addFieldToFilter('from_date',[['lteq' => $now_book->format('Y-m-d H:i:s')],['null' => true]])
					->addFieldToFilter('to_date',[['gteq' => $now_book->format('Y-m-d H:i:s')],['null' => true]]);
        return $book_data;
    }
}