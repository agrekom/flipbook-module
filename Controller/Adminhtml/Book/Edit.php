<?php
namespace Magebees\Flipbook\Controller\Adminhtml\Book;
use Magento\Backend\App\Action;
class Edit extends \Magento\Backend\App\Action
{
    protected $_coreRegistry = null;
    protected $resultPageFactory;
    protected $schedule_data = array();
 
    public function __construct(
        Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Registry $registry
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->_coreRegistry = $registry;
        parent::__construct($context);
    }
 
    protected function _initAction()
    {
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Magebees_Flipbook::flipbook');
        return $resultPage;
    }
        
    public function execute()
    {
        $id = $this->getRequest()->getParam('id');
        $model = $this->_objectManager->create('Magebees\Flipbook\Model\Book');
        if ($id) {
            $model->load($id);
            if (!$model->getId()) {
                $this->messageManager->addError(__('This rule record no longer exists.'));
                $resultRedirect = $this->resultRedirectFactory->create();
                return $resultRedirect->setPath('*/*/');
            }
        }
        $data = $this->_objectManager->get('Magento\Backend\Model\Session')->getFormData(true);
        if (!empty($data)) {
            $model->setData($data);
        }
        $this->_coreRegistry->register('books_data', $model);
        $resultPage = $this->_initAction();
        $resultPage->addBreadcrumb(
            $id ? __('Edit Book') : __('Add Book'),
            $id ? __('Edit Book') : __('Add Book')
        );
        $resultPage->getConfig()->getTitle()->prepend(__('MageBees Flipbook'));
        $resultPage->getConfig()->getTitle()->prepend(__('Flipbook'));
        $resultPage->getConfig()->getTitle()
            ->prepend($model->getId() ?  __('Edit Book') : __('Add Book'));
 
        return $resultPage;
    }
    
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Magebees_Flipbook::flipbook');
    }
}