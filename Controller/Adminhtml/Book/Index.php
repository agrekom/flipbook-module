<?php
namespace Magebees\Flipbook\Controller\Adminhtml\Book;
class Index extends \Magebees\Flipbook\Controller\Adminhtml\Book\Book
{
    public function execute()
    {
        $this->_initAction()->_addBreadcrumb(__('Flipbooks'), __('Flipbooks'));        
        $this->_view->getPage()->getConfig()->getTitle()->prepend(__('Manage Books'));
        $this->_view->getPage()->getConfig()->getTitle()->prepend(__('Flipbooks'));
        $this->_view->renderLayout('root');
    }
}