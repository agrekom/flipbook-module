<?php
namespace Magebees\Flipbook\Controller\Adminhtml\Book;
class MassStatus extends \Magento\Backend\App\Action
{
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magebees\Flipbook\Model\ProductFactory $bookproduct
    ) {
        $this->_bookproduct = $bookproduct;
        parent::__construct($context);
    }
    public function execute()
    {
        $bookIds = $this->getRequest()->getParam('books');   
        if (!is_array($bookIds) || empty($bookIds)) {
            $this->messageManager->addError(__('Please select book(s).'));
        } else {
            try {
                foreach ($bookIds as $bookId) {
                    $model = $this->_objectManager->get('Magebees\Flipbook\Model\Book')->load($bookId);
                    $data['product_ids'] = [];
                    $product_ids = $this->_bookproduct->create()->getCollection()->addFieldToFilter('book_id', $bookId);
                    foreach ($product_ids as $id){
                        $data['product_ids'][] = $id->getProductId();
                    }
                    $model->setProductIds($data['product_ids']);
                    $model->setStatus($this->getRequest()->getParam('status'))
                        ->setIsMassupdate(true)
                        ->save();
                }
                $this->messageManager->addSuccess(
                    __('Total of %1 record(s) were successfully updated.', count($bookIds))
                );
            } catch (\Exception $e) {
                $this->messageManager->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/');
    }

    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Magebees_Flipbook::flipbook');
    }
}
