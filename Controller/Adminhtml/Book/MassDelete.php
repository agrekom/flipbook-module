<?php
namespace Magebees\Flipbook\Controller\Adminhtml\Book;
class MassDelete extends \Magento\Backend\App\Action
{
    public function execute()
    {
        $bookIds = $this->getRequest()->getParam('books');        
        if (!is_array($bookIds) || empty($bookIds)) {
            $this->messageManager->addError(__('Please select book(s).'));
        } else {
            try {
                foreach ($bookIds as $bookId) {
                    $model = $this->_objectManager->get('Magebees\Flipbook\Model\Book')->load($bookId);
                    $model->delete();
                }                      
                    $this->messageManager->addSuccess(__('A total of %1 record(s) have been deleted.', count($bookIds)));
            } catch (\Exception $e) {
                $this->messageManager->addError($e->getMessage());
            }
        }
         $this->_redirect('*/*/');
    }
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Magebees_Flipbook::flipbook');
    }
}