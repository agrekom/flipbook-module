<?php
namespace Magebees\Flipbook\Controller\Adminhtml\Book;

class Grid extends \Magento\Backend\App\Action
{
    public function execute()
    {
        $this->getResponse()->setBody($this->_view->getLayout()->createBlock('Magebees\Flipbook\Block\Adminhtml\Book\Grid')->toHtml());
    }
    
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Magebees_Flipbook::flipbook');
    }
}
