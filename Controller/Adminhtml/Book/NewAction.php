<?php
namespace Magebees\Flipbook\Controller\Adminhtml\Book;
class NewAction extends \Magento\Backend\App\Action
{
    public function execute()
    {
        $this->_forward('edit');
    }
}
