<?php
namespace Magebees\Flipbook\Controller\Adminhtml\Book;
abstract class Book extends \Magento\Backend\App\Action
{
    protected $coreRegistry = null;
    protected $fileFactory;
    protected $dateFilter;
    protected $bookFactory;
    protected $logger;
	
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Framework\App\Response\Http\FileFactory $fileFactory,
        \Magento\Framework\Stdlib\DateTime\Filter\Date $dateFilter,
        \Magebees\Flipbook\Model\BookFactory $bookFactory,
        \Psr\Log\LoggerInterface $logger
    ) {
        parent::__construct($context);
        $this->coreRegistry = $coreRegistry;
        $this->fileFactory = $fileFactory;
        $this->dateFilter = $dateFilter;
        $this->bookFactory = $bookFactory;
        $this->logger = $logger;
    }
    protected function _initRule()
    {
        $rule = $this->bookFactory->create();
        $this->coreRegistry->register('current_book',$rule);
        $id = (int)$this->getRequest()->getParam('id');
        if (!$id && $this->getRequest()->getParam('book_id')) {
            $id = (int)$this->getRequest()->getParam('book_id');
        }
        if ($id) {
            $this->coreRegistry->registry('current_book')->load($id);
        }
    }
    protected function _initAction()
    {
        $this->_view->loadLayout();
        $this->_setActiveMenu('Magebees_Flipbook::flipbook')
            ->_addBreadcrumb(__('Flipbooks'), __('Flipbooks'));
        return $this;
    }
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Magebees_Flipbook::flipbook');
    }
}