<?php
namespace Magebees\Flipbook\Controller\Adminhtml\Book;
class ImageDelete extends \Magento\Backend\App\Action
{
    public function execute()
    {
        $id = $this->getRequest()->getParam('imageId');
        if ($id) {
            try {
                $model = $this->_objectManager->create(\Magebees\Flipbook\Model\Bookimage::class);
                $model->load($id);
                $model->delete();
                $response = $this->resultFactory->create(\Magento\Framework\Controller\ResultFactory::TYPE_JSON)
                    ->setData(
                        [
                        'status'  => "ok",
                        'message' => "image deleted"
                        ]
                    );

                return $response;
                //return true;
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $response = $this->resultFactory->create(\Magento\Framework\Controller\ResultFactory::TYPE_JSON)
                    ->setData(
                        [
                        'status'  => false,
                        'message' => "image not deleted"
                        ]
                    );
                return $response;
            }
        }
    }
}