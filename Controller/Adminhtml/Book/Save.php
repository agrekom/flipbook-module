<?php
namespace Magebees\Flipbook\Controller\Adminhtml\Book;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\App\Filesystem\DirectoryList;
class Save extends \Magento\Backend\App\Action
{
    protected $_mediaDir = 'magebees/flipbook/';
    protected $_bookproduct;
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Framework\App\Request\DataPersistorInterface $dataPersistor,
        \Magebees\Flipbook\Model\BookimageFactory $bookImageFactory,
        \Magebees\Flipbook\Model\ProductFactory $bookproduct,
        \Magento\Framework\App\Filesystem\DirectoryList $directoryList
    ) {
        $this->coreRegistry = $coreRegistry;
        $this->dataPersistor = $dataPersistor;
        $this->bookImageFactory = $bookImageFactory;
        $this->_bookproduct = $bookproduct;
        $this->directory_list = $directoryList;
        parent::__construct($context);
    }

    public function execute()
    {
        $data = $this->getRequest()->getPost()->toArray();
        $id = $this->getRequest()->getParam('book_id');
        if ($data) {
			if(isset($data['position_on_product_page'])){
				$data['position_on_product_page'] = implode(',',$data['position_on_product_page']);
			}
			if(isset($data['categories'])){
				$data['categories'] = implode(',',$data['categories']);
			}else{
				$data['categories'] = "";
			}
            $book_data = $this->_objectManager->create('Magebees\Flipbook\Model\Book');
            if ($id) {
                $book_data->load($id);
            }
            try{
                $uploded_file = $this->getRequest()->getFiles()->toArray();
                if(!empty($uploded_file['thumbnail_book']) && !empty($uploded_file['thumbnail_book']['name'])) {
                    try {
                        $uploader = $this->_objectManager->create(
                            'Magento\MediaStorage\Model\File\Uploader',
                            ['fileId' => 'thumbnail_book']
                        );
                        $uploader->setAllowedExtensions(['jpg', 'jpeg', 'gif', 'png']);
                        $imageAdapter = $this->_objectManager->get('Magento\Framework\Image\AdapterFactory')->create();
                        $uploader->addValidateCallback('flipbook_thumbnail', $imageAdapter, 'validateUploadFile');
                        $uploader->setAllowRenameFiles(true);
                        $uploader->setFilesDispersion(false);
                        $mediaDirectory = $this->_objectManager->get('Magento\Framework\Filesystem')
                            ->getDirectoryRead(DirectoryList::MEDIA);
                        $result = $uploader->save($mediaDirectory->getAbsolutePath($this->_mediaDir));
                        $data['thumbnail_book'] = $this->_mediaDir . $result['file'];
                    } catch (\Exception $e) {
                        if ($e->getCode() == 0) {
                            $data['fail'] = $e->getMessage();
                            $this->messageManager->addError($e->getMessage());
                        }
                    }
                }
                
                $jsHelper = $this->_objectManager->create('Magento\Backend\Helper\Js');
                if(!empty($data['links']['book'])) {
                    $data['product_ids'] = $jsHelper->decodeGridSerializedInput($data['links']['book']);
                }else{
                    $data['product_ids'] = [];
                    $product_ids = $this->_bookproduct->create()->getCollection()->addFieldToFilter('book_id',$id);

                    foreach ($product_ids as $id){
                        $data['product_ids'][] = $id->getProductId();
                    }
                }
                if(!empty($uploded_file['cover_book'])) {
                    $uploded_book_cover = $uploded_file['cover_book'];
                }
                if(!empty($uploded_book_cover) && !empty($uploded_book_cover['name'])) {
                    try {   
                        $uploader = $this->_objectManager->create(
                            'Magento\MediaStorage\Model\File\Uploader',
                            ['fileId' => 'cover_book']
                        );
                        $uploader->setAllowedExtensions(['jpg', 'jpeg', 'gif', 'png']);
                        $imageAdapter = $this->_objectManager->get('Magento\Framework\Image\AdapterFactory')->create();
                        $uploader->addValidateCallback('flipbook_cover_book', $imageAdapter, 'validateUploadFile');
                        $uploader->setAllowRenameFiles(true);
                        $uploader->setFilesDispersion(false);
                        $mediaDirectory = $this->_objectManager->get('Magento\Framework\Filesystem')
                            ->getDirectoryRead(DirectoryList::MEDIA);
                        $result = $uploader->save($mediaDirectory->getAbsolutePath($this->_mediaDir));
                        $data['cover_book'] = $this->_mediaDir . $result['file'];
                    } catch (\Exception $e) {
                        if ($e->getCode() == 0) {
                            $data['fail'] = $e->getMessage();
                            $this->messageManager->addError($e->getMessage());
                        }
                    }
                }
                if(!empty($uploded_file['last_book'])) {
                    $uploded_book_last = $uploded_file['last_book'];
                
                }
                if(!empty($uploded_book_last) && !empty($uploded_book_last['name'])) {
                    try {
                        
                        $uploader = $this->_objectManager->create(
                            'Magento\MediaStorage\Model\File\Uploader',
                            ['fileId' => 'last_book']
                        );
                        $uploader->setAllowedExtensions(['jpg', 'jpeg', 'gif', 'png']);
                        $imageAdapter = $this->_objectManager->get('Magento\Framework\Image\AdapterFactory')->create();
                        $uploader->addValidateCallback('flipbook_last_book', $imageAdapter, 'validateUploadFile');
                        $uploader->setAllowRenameFiles(true);
                        $uploader->setFilesDispersion(false);
                        $mediaDirectory = $this->_objectManager->get('Magento\Framework\Filesystem')
                            ->getDirectoryRead(DirectoryList::MEDIA);
                        $result = $uploader->save($mediaDirectory->getAbsolutePath($this->_mediaDir));
                        $data['last_book'] = $this->_mediaDir . $result['file'];                    
                    } catch (\Exception $e) {
                        if ($e->getCode() == 0) {
                            $data['fail'] = $e->getMessage();
                            $this->messageManager->addError($e->getMessage());
                        }
                    }
                }
                if(!empty($uploded_file['book_pdf']) && !empty($uploded_file['book_pdf']['name']) && $data['mode_of_upload'] ==='1') {
                    try {       
                        $uploader = $this->_objectManager->create(
                            'Magento\MediaStorage\Model\File\Uploader',
                            ['fileId' => 'book_pdf']
                        );
                             $uploader->setAllowedExtensions(['pdf']);
                             $uploader->setAllowRenameFiles(true);
                             $uploader->setFilesDispersion(false);
                             $mediaDirectory = $this->_objectManager->get('Magento\Framework\Filesystem')->getDirectoryRead(DirectoryList::MEDIA);
                            $result = $uploader->save($mediaDirectory->getAbsolutePath($this->_mediaDir));
                            $data['book_pdf'] = $this->_mediaDir . $result['file'];
                    } catch (\Exception $e) {
                        if ($e->getCode() == 0) {
                            $data['fail'] = $e->getMessage();
                            $this->messageManager->addError($e->getMessage());
                        }
                    }
                }
                if($id) {
                    $uploded_images = [];
                    if(!empty($data['image_id']) && !empty($data['uploaded_image_sort_order']) && !empty($data['uploaded_image_notes']) ) {
                        foreach ($data['image_id'] as $key => $id) {
                            $uploded_images[$key]['image_id'] = $id;                        
                        }
                        foreach ($data['uploaded_image_sort_order'] as $key => $val) {
                            $uploded_images[$key]['sort_order'] = $val;                        
                        }
                        foreach ($data['uploaded_image_notes'] as $key => $val) {
                            $uploded_images[$key]['notes'] = $val;                        
                        }
                        foreach ($uploded_images as $image) {
                            $image_data = $this->bookImageFactory->create()->load($image['image_id']);
                            $image_data->setData('sort_order', $image['sort_order']);
                            $image_data->setData('notes', $image['notes']);
                            $image_data->save();
                        }
                    }    
                }
                if(empty($data['fail'])) {
                    $data['description'] = trim($data['description']);
                    $data['author_name'] = trim($data['author_name']);
                    $data['book_title'] = trim($data['book_title']);
                    $book_data->setData($data);
                    $book_data->save();
                    $this->messageManager->addSuccess(__('Book was successfully saved'));
                }
                $this->_objectManager->get('Magento\Backend\Model\Session')->setFormData(false);
                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', ['id' => $book_data->getBookId(), '_current' => true]);
                    return;
                }
                $this->_redirect('*/*/');
                return;
            } catch (\Magento\Framework\Model\Exception $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\RuntimeException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addException($e, __('Something went wrong while saving the book.'));
                $this->messageManager->addError($e->getMessage());
            }
            $this->_getSession()->setFormData($data);
            $this->_redirect('*/*/edit', ['id' => $this->getRequest()->getParam('book_id')]);
            return;
        }
        $this->_redirect('*/*/');
    }  
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Magebees_Flipbook::flipbook');
    }
}