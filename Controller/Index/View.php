<?php
namespace Magebees\Flipbook\Controller\Index;
use Magento\Framework\Exception\NotFoundException;
use Magento\Framework\App\RequestInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Framework\App\Action\Action;

class View extends \Magento\Framework\App\Action\Action
{
    const XML_PATH_ENABLED = 'flipbook/general/enabled';
    protected $scopeConfig;
    protected $resultPageFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ) {
    
        parent::__construct($context);
        $this->scopeConfig = $scopeConfig;
        $this->_config = $scopeConfig->getValue('flipbook/general', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $this->resultPageFactory = $resultPageFactory;
    }

    public function dispatch(RequestInterface $request)
    {
        if (!$this->scopeConfig->isSetFlag(self::XML_PATH_ENABLED, ScopeInterface::SCOPE_STORE)) {
            throw new NotFoundException(__('Page not found.'));
        }
        return parent::dispatch($request);
    }
    
    public function execute()
    {
        //$this->_view->getPage()->getConfig()->getTitle()->set('Flipbooks');
        return $this->resultPageFactory->create();
    }
}
