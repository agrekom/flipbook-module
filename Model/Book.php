<?php
namespace Magebees\Flipbook\Model;
class Book extends \Magento\Framework\Model\AbstractModel
{
    protected function _construct()
    {
        parent::_construct();
        $this->_init('Magebees\Flipbook\Model\ResourceModel\Book');
        $this->setIdFieldName('book_id');
    }  
}
