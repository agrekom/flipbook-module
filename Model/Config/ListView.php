<?php
namespace Magebees\Flipbook\Model\Config;
class ListView implements \Magento\Framework\Option\ArrayInterface
{
    public function toOptionArray()
    {
        return [['value' =>'1', 'label' => __('Full')],['value' =>'2', 'label' => __('Half')]];
    }

    public function toArray()
    {
        return [1 => __('Half'),2=>__('Full')];
    }
}
