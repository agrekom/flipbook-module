<?php
namespace Magebees\Flipbook\Model\Config;
class GridCol implements \Magento\Framework\Option\ArrayInterface
{
    public function toOptionArray()
    {
        return [['value' =>'2', 'label' => __('2')],['value' =>'3', 'label' => __('3')],['value' =>'4', 'label' => __('4')]];
    }

    public function toArray()
    {
        return [1 => __('2'),2=>__('3'),2=>__('4')];
    }
}
