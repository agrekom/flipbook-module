<?php

namespace Magebees\Flipbook\Model\ResourceModel;

class Book extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    protected $_productFactory;
    protected $_mediaDir = 'magebees/flipbook/bookimages/';
    protected \Magento\Framework\App\Request\Http $request;
    protected \Magebees\Flipbook\Model\BookimageFactory $bookImageFactory;
    protected \Magento\Framework\Message\ManagerInterface $messageManager;
    protected \Magebees\Flipbook\Model\ProductFactory $bookProduct;

    public function __construct(
        \Magento\Framework\Model\ResourceModel\Db\Context $context,
        \Magento\Framework\App\Request\Http $request,
        \Magebees\Flipbook\Model\BookimageFactory $bookImageFactory,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Magebees\Flipbook\Model\ProductFactory $bookProduct
    ) {
        parent::__construct($context);
        $this->request = $request;
        $this->bookImageFactory = $bookImageFactory;
        $this->messageManager = $messageManager;
        $this->bookProduct = $bookProduct;
    }
    protected function _construct()
    {
        $this->_init('magebees_flip_book', 'book_id');
    }

    public function _afterSave(\Magento\Framework\Model\AbstractModel $object)
    {
        $id = $object->getBookId();
        $this->_objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $uploded_file_ = $this->request->getFiles()->toArray();
        if(isset($uploded_file_['flipbook_image_book_image'])) {
            $image_uploded_data = $uploded_file_['flipbook_image_book_image'];
        }
        $data = $object->getData();
        if(isset($data['flipbook_image']) && !empty($data['flipbook_image'])) {
            foreach ($data['flipbook_image'] as $dkey => $value) {
                $image_uploded_data[$dkey]['sort_order'] = trim((string) $value['sort_order']);
                $image_uploded_data[$dkey]['notes'] = trim((string) $value['notes']);
                $image_uploded_data[$dkey]['book_id'] = $id;
            }
        }
        if(!empty($image_uploded_data)) {
            foreach ($image_uploded_data as $ukey => $image) {
                try {
                    $uploader = $this->_objectManager->create(
                        'Magento\MediaStorage\Model\File\Uploader',
                        ['fileId' => 'flipbook_image_book_image['.$ukey.']']
                    );
                    $uploader->setAllowedExtensions(['jpg', 'jpeg', 'gif', 'png']);
                    $imageAdapter = $this->_objectManager->get('Magento\Framework\Image\AdapterFactory')->create();
                    $uploader->addValidateCallback('flipbook_image_book_image', $imageAdapter, 'validateUploadFile');
                    $uploader->setAllowRenameFiles(true);
                    $uploader->setFilesDispersion(false);
                    $mediaDirectory = $this->_objectManager->get('Magento\Framework\Filesystem')
                        ->getDirectoryRead(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA);
                    $result = $uploader->save($mediaDirectory->getAbsolutePath($this->_mediaDir));
                    $image_book_path = $this->_mediaDir . $result['file'];
                    $book_image_model = $this->bookImageFactory->create();
                    $image['image'] = $image_book_path;
                    $book_image_model->setData($image);
                    $book_image_model->save();
                } catch (\Exception $e) {
                    if ($e->getCode() == 0) {
                            $object->setData('fail', $e->getMessage());
                            $data['fail'] = $e->getMessage();
                            $this->messageManager->addError($e->getMessage());
                    }
                }
            }
        }
        $book_product_model = $this->bookProduct->create();
        if($id) {
            $product_data = $book_product_model->getCollection()->addFieldToFilter('book_id', $id);
            $product_data->walk('delete');
            $product_book = array();
            if(!empty($data['product_ids'])) {
                foreach($data['product_ids'] as $product){
                    $product_book['book_id'] = $data['book_id'];
                    $product_book['product_id'] = $product;
                    $book_product_model->setData($product_book);
                    $book_product_model->save();
                }
            }
        }
        return parent::_afterSave($object);
    }
}
