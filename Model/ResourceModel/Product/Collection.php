<?php
namespace Magebees\Flipbook\Model\ResourceModel\Product;
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected function _construct()
    {
        $this->_init('Magebees\Flipbook\Model\Product', 'Magebees\Flipbook\Model\ResourceModel\Product');
    }
}
