<?php
namespace Magebees\Flipbook\Model\ResourceModel\Bookimage;
class Collection  extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected function _construct()
    {
        $this->_init('Magebees\Flipbook\Model\Bookimage', 'Magebees\Flipbook\Model\ResourceModel\Bookimage');
    }
}
