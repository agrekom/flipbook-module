<?php
namespace Magebees\Flipbook\Model\ResourceModel\Book;
class Collection  extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected function _construct()
    {
        $this->_init('Magebees\Flipbook\Model\Book', 'Magebees\Flipbook\Model\ResourceModel\Book');
    }
}
