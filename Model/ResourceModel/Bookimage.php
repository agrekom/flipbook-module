<?php
namespace Magebees\Flipbook\Model\ResourceModel;
class Bookimage extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    protected function _construct()
    {
        $this->_init('magebees_flip_book_images', 'image_id');
    }
}