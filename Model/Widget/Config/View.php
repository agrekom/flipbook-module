<?php
namespace Magebees\Flipbook\Model\Widget\Config;
class View implements \Magento\Framework\Option\ArrayInterface
{
    public function toOptionArray()
    {
        return [['value' =>'list', 'label' => __('List View')],['value' =>'grid', 'label' => __('Grid View')]];
    }

    public function toArray()
    {
        return [1 => __('Grid View'),2=>__('List View')];
    }
}
