<?php
namespace Magebees\Flipbook\Model\Widget\Config;
class Book implements \Magento\Framework\Option\ArrayInterface
{
    protected $_bookFactory;
    public function __construct(
        \Magebees\Flipbook\Model\BookFactory $bookFactory
    ) {
        $this->_bookFactory = $bookFactory;
    }
    public function toOptionArray()
    {
        $conllection = $this->_bookFactory->create()->getCollection();
        $options =[];
        foreach ($conllection as $item) {
            $options[] = array(
                            'label' => $item->getBookTitle(),
                            'value' => $item->getBookId()
            );
        }
        return $options;
    }
}