<?php
namespace Magebees\Flipbook\Model\Widget\Config;
class Booktype implements \Magento\Framework\Option\ArrayInterface
{
    protected $_bookFactory;
    public function __construct(
        \Magebees\Flipbook\Model\BookFactory $bookFactory
    ) {
        $this->_bookFactory = $bookFactory;
    }
    public function toOptionArray()
    {
        $options =[];
		$options[0] = array(
			'label' => "Grid",
			'value' => "grid"
		);
		$options[1] = array(
			'label' => "List",
			'value' => "list"
		);
        return $options;
    }
}