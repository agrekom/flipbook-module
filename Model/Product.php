<?php
namespace Magebees\Flipbook\Model;
class Product extends \Magento\Framework\Model\AbstractModel
{
    protected function _construct()
    {
        $this->_init('Magebees\Flipbook\Model\ResourceModel\Product');
    }
}
