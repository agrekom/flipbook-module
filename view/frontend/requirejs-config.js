define('three', ['Magebees_Flipbook/js/plugin/three.min'], function (THREE) {
    window.THREE = THREE;
    return THREE;
});
var config = {
    map: {
        '*': {
            'html2canvas': 'Magebees_Flipbook/js/plugin/html2canvas.min'
        },
    },
    paths: {
        'three'     : 'Magebees_Flipbook/js/plugin/three.min',
        'pdfjs-dist': 'Magebees_Flipbook/js/plugin/pdfjs-dist',
        'lightbox'  : 'Magebees_Flipbook/js/plugin/lightbox',
        'flipbook'  : 'Magebees_Flipbook/js/plugin/3dflipbook.min'
    },
    shim: {
        'html2canvas': {
            deps: ['jquery']
        },
        'three': {
           exports: 'THREE'
        },
        'flipbook': {
            deps: ['jquery','html2canvas', 'three']
        }
    }
};