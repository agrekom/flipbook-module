define(
    [
    "jquery",
    "mage/template",
    "Magento_Ui/js/modal/confirm",
    "prototype"
    ], function ($,mageTemplate,confirm) {
        var flipbook_images = {
            template: null,
            index : 0,
            
            initialize: function () {
                
                this.template = mageTemplate('#flipbook_image_row_template');
                
            },            
            add : function () {
                this.index++;
                var data = {index:this.index};
                Element.insert(
                    $('flipbook_image_row_container'), {
                        bottom: this.template(
                            {
                                data: data
                            }
                        )
                    }
                );
                
            }
         
        }

        return flipbook_images

    }
);
