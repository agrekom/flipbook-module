<?php
namespace Magebees\Flipbook\Helper;
use Magento\Store\Model\ScopeInterface;
use Magento\Framework\View\Asset\Repository;
use Magento\Framework\App\RequestInterface; // for $this->result

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    protected $request;
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magebees\Flipbook\Model\BookimageFactory $bookImageFactory,
        Repository $assetRepo,
        RequestInterface $request,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig 
    ) {
        $this->_storeManager = $storeManager;
        $this->bookImageFactory= $bookImageFactory;
        $this->_scopeConfig = $scopeConfig;
        $this->assetRepo = $assetRepo;
        $this->request = $request;
        parent::__construct($context);
    }

    public function isEnabled()
    {
        return $this->_scopeConfig->getValue('flipbook/general/enabled', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
	
    public function getFlipbookViewType()
    {
        return $this->_scopeConfig->getValue('flipbook/general/view_type', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    public function getGridCol()
    {
        return $this->_scopeConfig->getValue('flipbook/general/grid_col', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    public function getListView()
    {
        return $this->_scopeConfig->getValue('flipbook/general/list_view', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    public function getLetterCount()
    {
        return $this->_scopeConfig->getValue('flipbook/general/letter_count', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    public function getFlipLetterCount()
    {
        return $this->_scopeConfig->getValue('flipbook/general/flip_letter_count', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    public function getMediaUrl()
    {
        $currentStore = $this->_storeManager->getStore();
        $mediaUrl = $currentStore->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
        return $mediaUrl;
    }
	
	public function getBaseUrl()
    {
        $currentStore = $this->_storeManager->getStore();
        $baseUrl = $currentStore->getBaseUrl();
        return $baseUrl;
    }

    public function getUniqueSliderKey()
    {
        $key = uniqid();
        return $key;
    }

    public function getThumbnailImage($file)
    {
        $mediaDirectory = $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA); 
        $thumbnail_Path = $file['thumbnail_book'];
        return $mediaDirectory.$thumbnail_Path;
    }

    public function getUploadedImages($book)
    {
        $book_id = $book['book_id'];
        $images_data = $this->bookImageFactory->create()->getCollection()->addFieldToFilter("book_id", $book_id);
        $images_data = $images_data->setOrder('sort_order', 'ASC')->getData();
        $mediaUrl = $this->getMediaUrl();
        $firstImage = Array('0' => ['book_id' => $book_id,
                          'image' => $book['cover_book'],
                          'sort_order' => '0',
                          'notes' => '']);
    
        $lastImage = Array('book_id' => $book_id,
                      'image' => $book['last_book'],
                      'sort_order' => '0',
                      'notes' => '');

        $newUpdatedImages = [];
        $newUpdatedImages = array_merge($newUpdatedImages, $images_data);
        if(!empty($book['cover_book'])) {
            $newUpdatedImages = array_merge($firstImage, $images_data);
        }      
        if(!empty($book['last_book'])) {  
            $newUpdatedImages[] = $lastImage;
        }
        $img_cnt = count($newUpdatedImages);
        if($img_cnt==2) {
            $params = array('_secure' => $this->request->isSecure());
            $img_extra = $this->assetRepo->getUrlWithParams('Magebees_Flipbook::images/no-img.jpg', $params);
            $conditionImage = Array('book_id' => $book_id,
                    'image' => $img_extra,
                    'sort_order' => '',
                    'notes' => '');

            $newUpdatedImages[] = $conditionImage;
            $newUpdatedImages[] = $conditionImage;
        }
        if($img_cnt%2!==0) {
            $params = array('_secure' => $this->request->isSecure());
            $img_extra = $this->assetRepo->getUrlWithParams('Magebees_Flipbook::images/no-img.jpg', $params);
            
            $conditionImage = Array('book_id' => $book_id,
                    'image' => $img_extra,
                    'sort_order' => '',
                    'notes' => '');

            $newUpdatedImages[] = $conditionImage;
        }      
        return $newUpdatedImages;
    }

    public function getPdf($file)
    {
        $mediaDirectory = $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
        $pdf_Path = $file['book_pdf'];
        return $mediaDirectory.$pdf_Path;
    }

    public function getCountsofbook($booksData,$type)
    {
        $getCountsofbook = [];
        foreach ($booksData as $bookd) {
            $now = new \DateTime();
            $currentdate = $now->format("y-m-d");
            $statusd = $bookd['status'];
            $add_to_productd = $bookd['add_product'];
            $add_product_posd = explode(",",$bookd['position_on_product_page']);
        
            if(!empty($bookd['from_date']) && !empty($bookd['to_date'])) {    
                $fromDate = new \DateTime($bookd['from_date']);
                $fromDate = $fromDate->format("y-m-d");
                $toDate = new \DateTime($bookd['to_date']);
                $toDate = $toDate->format("y-m-d");
          
                if($statusd && $add_to_productd && (in_array($type, $add_product_posd))) {
                    if($currentdate > $fromDate && $currentdate < $toDate) {
                        $getCountsofbook[] = 1;
                    }
                }
            } else {
                if($statusd && $add_to_productd && (in_array($type, $add_product_posd))) {
                    $getCountsofbook[] = 1; 
                }
            }
        }
        return $getCountsofbook; 
    }

    public function isDateConditionCheck($book)
    {
        if(!empty($book['from_date']) && !empty($book['to_date'])) { 
            $now = new \DateTime();
            $currentdate = $now->format("y-m-d");     
            $fromDate = new \DateTime($book['from_date']);
            $fromDate = $fromDate->format("y-m-d");
            $toDate = new \DateTime($book['to_date']);
            $toDate = $toDate->format("y-m-d");

            if($currentdate > $fromDate && $currentdate < $toDate) {
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }  
    }
}